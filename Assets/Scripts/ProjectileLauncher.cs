using UnityEditor.Callbacks;
using UnityEngine;

public class ProjectileLauncher : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform launchPoint;
    public float launchForce = 10f;
    public Transform target;  // objetivo en la escena
    [SerializeField]private Rigidbody2D rb;
    private float axisY;
    [SerializeField]private float minY,maxY;
    public int speed; 

    void Update()
    {
        axisY=Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector3(rb.velocity.x, speed*axisY);
        transform.position=new Vector2((transform.position.x),Mathf.Clamp(transform.position.y,minY,maxY));
        if (Input.GetButtonDown("Fire1"))
        {
            LaunchProjectile();
        }

        CheckProximityToTarget();
    }

    void LaunchProjectile()
    {
        // Crear el proyectil
        GameObject projectile = Instantiate(projectilePrefab, launchPoint.position, launchPoint.rotation);

        // Obtener la dirección de lanzamiento  (VECTORES)
        Vector3 launchDirection = GetLaunchDirection();

        // Aplicar la fuerza al proyectil
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.AddForce(launchDirection * launchForce, ForceMode2D.Impulse);
    }

    Vector3 GetLaunchDirection()
    {
        // Obtener la posición del mouse en el mundo(ARITMETICA BASICA)
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Camera.main.transform.position.z;
        Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        // Calcular el vector de dirección desde el punto de lanzamiento hacia el mouse
        Vector3 direction = worldMousePosition - launchPoint.position;

        // Normalizar el vector para obtener solo la dirección(NORMALIZACIÓN y MAGNITUD) MAGNITUD=1
        direction = direction.normalized;
        return direction;
    }

    void CheckProximityToTarget()
    {
        // Comprobar si el objetivo está asignado
        if (target == null) return;

        // Calcular la distancia entre el proyectil y el objetivo(COMPARACIÓN DE VECTORES)
        float distance = Vector3.Distance(launchPoint.position, target.position);

        // Umbral para considerar que el proyectil está cerca del objetivo
        float proximityThreshold = 1.0f;

        if (distance < proximityThreshold)
        {
            Debug.Log("El proyectil está cerca del objetivo!");
        }
    }
}
